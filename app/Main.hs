module Main where

import           Lib                          (Sierpinski, SimulationState (..))

import           Graphics.Gloss               (Color, Display (InWindow), black,
                                               simulate)
import           Graphics.Gloss.Data.ViewPort (ViewPort)

import           System.Random

backgroundColor :: Color
backgroundColor = black

windowSize :: (Int, Int)
windowSize = (800, 800)

windowPos :: (Int, Int)
windowPos = (100, 100)

windowTitle :: String
windowTitle = "Chaos Game"

main :: IO ()
main = simulate window backgroundColor fps Lib.init render step
  where
    window :: Display
    window = InWindow windowTitle windowSize windowPos

    fps :: Int
    fps = 300

    step :: ViewPort -> Float -> Sierpinski -> Sierpinski
    step _ _ = update
