module Lib
    ( SimulationState (..)
    , Sierpinski
    ) where

import           Sierpinski      (Sierpinski)
import           SimulationState (SimulationState (..))
