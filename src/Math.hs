module Math
  ( fromDegrees
  , genSeed
  , radians
  , randomPick
  ) where

import           Graphics.Gloss (Point)
import           System.Random  (RandomGen, randomR)

newtype Radians = Radians Float

radians :: Float -> Radians
radians angle = Radians angle

fromDegrees :: Float -> Radians
fromDegrees degrees = Radians $ degrees * pi / 180

toFloat :: Radians -> Float
toFloat (Radians flt) = flt

randomPick :: RandomGen rng => rng -> [a] -> (a, rng)
randomPick rng lst =
  (lst !! randomIdx, newRng)

  where
    randomIdx :: Int
    (randomIdx, newRng) = randomR (0, length lst - 1) rng

genSeed :: Int -> Float -> Radians -> [Point]
genSeed numPoints radius startAngle =
  genSeed' numPoints (toFloat startAngle) []

  where
    singleRotation :: Float -> Float
    singleRotation ang =
      if ang >= 0 && ang <= 2*pi then
        ang
      else
        singleRotation $ ang - 2*pi

    incrementalAngle :: Float
    incrementalAngle = 2*pi / (fromIntegral numPoints)

    angleToPoint :: Float -> Point
    angleToPoint ang = ( radius * cos ang
                       , radius * sin ang
                       )

    succAngle :: Float -> Float
    succAngle ang = ang + incrementalAngle

    genSeed' :: Int -> Float -> [Point] -> [Point]
    genSeed' remaining angle acc
      | remaining < 1 = acc
      | otherwise     = genSeed' (pred remaining) (succAngle angle) $ (angleToPoint angle) : acc
