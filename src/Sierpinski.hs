module Sierpinski
  ( Sierpinski
  ) where

import           Math            (fromDegrees, genSeed, randomPick)
import           SimulationState

import           Graphics.Gloss  (Point)
import           System.Random   (StdGen, mkStdGen)

lerpfactor :: Float
lerpfactor = 1/2

data Sierpinski = Sierpinski
  { seed :: [Point]
  , pts  :: [Point]
  , rng  :: StdGen
  , prev :: Point
  , curr :: Point
  }

newSierpinski :: Sierpinski
newSierpinski = Sierpinski
  { seed = seed
  , pts  = []
  , rng  = rng
  , prev = startingPoint
  , curr = (0, 0)
  }

  where
    seed :: [Point]
    seed = genSeed 3 400 (fromDegrees 90)

    startingPoint :: Point
    (startingPoint, rng) = randomPick (mkStdGen 1) seed

lerp :: Float -> Float -> Float -> Float
lerp start end factor = factor * (end - start) + start

lerpPoint :: Point -> Point -> Float -> Point
lerpPoint (startX, startY) (endX, endY) factor =
  ( lerp startX endX factor
  , lerp startY endY factor
  )

updateSierpinski :: Sierpinski -> Sierpinski
updateSierpinski state = state
  { pts  = newpoint : pts
  , rng  = newRng
  , prev = target
  , curr = newpoint
  }

  where
    Sierpinski { seed = seed
               , pts  = pts
               , rng  = rng
               , prev = prev
               , curr = curr
               } = state

    (target, newRng) = randomPick rng seed

    newpoint :: Point
    newpoint = lerpPoint curr target lerpfactor

instance SimulationState Sierpinski where
  init = newSierpinski

  points state = seed state ++ pts state

  update = updateSierpinski
