module SimulationState where

import           Graphics.Gloss (Picture, Point, circleSolid, color, pictures,
                                 translate, white)

pointSize :: Float
pointSize = 1

class SimulationState state where
  init :: state

  points :: state -> [Point]

  render :: state -> Picture
  render state = color white
               $ pictures
               $ map drawPoint (points state)
    where
      drawPoint :: Point -> Picture
      drawPoint (x, y) = translate x y
                       $ circleSolid pointSize

  update :: state -> state
  update = id
